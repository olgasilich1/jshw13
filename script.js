const button = document.querySelector('.change-button');
const link = document.querySelector('.styles-link');

let flag = false;
let styles = 'css/styles.css';

if (localStorage.getItem('styles-changed')) {
    styles = localStorage.getItem('styles-changed');
    flag = true;
}

link.setAttribute('href', styles);

button.addEventListener('click', (e) => {
    localStorage.clear();
    if (!flag) {
        link.setAttribute('href', 'css/styles-changed.css');
        localStorage.setItem('styles-changed', 'css/styles-changed.css');
    } else {
        link.setAttribute('href', 'css/styles.css');
        localStorage.setItem('styles', 'css/styles.css');
    }
    flag = !flag;
})